package com.example.uttam.persistentbottomslide;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private BottomSheetBehavior mBottomSheetBehavior;
    private TextView mTextViewState;
    ImageView collapsingImage;
    int p=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        View view=null;

        //makeCircularRevealAnimation(view);

        collapsingImage=findViewById(R.id.collapsingImageId);
        View bottomSheet = findViewById(R.id.bottom_sheet);

        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);

        mTextViewState = findViewById(R.id.text_view_state);

//        Button buttonExpand = findViewById(R.id.button_expand);
//        Button buttonCollapse = findViewById(R.id.button_collapse);
//
//        buttonExpand.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//            }
//        });
//
//        buttonCollapse.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//            }
//        });

        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        mTextViewState.setText("Collapsed");
                        p=0;
                        collapsingImage.setImageResource(R.drawable.ic_expand);
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        p=1;
                        mTextViewState.setText("Dragging...");
                        collapsingImage.setImageResource(R.drawable.ic_dragging_dot);
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        p=1;
                        mTextViewState.setText("Expanded");
                        collapsingImage.setImageResource(R.drawable.ic_expand_less);
                        break;
//                    case BottomSheetBehavior.STATE_HIDDEN:
//                        mTextViewState.setText("Hidden");
//                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        p=1;
                        mTextViewState.setText("Settling...");
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                p=1;
                mTextViewState.setText("Sliding...");
            }
        });
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if(p==0){
            super.onBackPressed();
        }else{
            p=0;
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }


//    private void makeCircularRevealAnimation(final View view) {
//
//        final int centerX = (view.getLeft() + view.getRight())/2;
//        int centerY = (view.getTop() + view.getBottom())/2;
//
//        float radius = Math.max(view.getWidth(), view.getHeight()) * 2.0f;
//        Animator reveal = ViewAnimationUtils.createCircularReveal(view, centerX, centerY, radius, 0);
//        reveal.addListener(new AnimatorListenerAdapter(){
//            public void onAnimationEnd(Animator animation) {
//                view.setVisibility(View.INVISIBLE);
//            }
//        });
//        reveal.start();
//
//    }
}